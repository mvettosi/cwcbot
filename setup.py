from setuptools import setup

setup(name='cwcbot',
      version='0.1',
      description='CwC bot',
      url='https://gitlab.com/mvettosi/cwcbot',
      author='Matteo Vettosi',
      author_email='matteo.vettosi@gmail.com',
      license='MIT',
      packages=['cwcbot'],
      zip_safe=False)
